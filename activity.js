db.fruits.aggregate
(
	[
		{
            $group: 
			{ 
                _id: "$onSale",

            }
        },

        {
        	$count: "true"
        }
	]
);

db.fruits.aggregate
(
	[

		{
			$match:
			{
				stock: {gt:20}
			}
		},

		{
            $group: 
			{ 
                _id: "$stock"
                      
            }

            {
        	$count: "true"
        	}
        }
	]
);


db.fruits.aggregate
([
	{
		$group:
		{
			_id:"$supplier_id",
			avePrice:{$avg:"$price"}
				
		}
	}
]);

db.fruits.aggregate
([
	{
		$group:
		{
			_id:"$supplier_id",
			maxPrice:{$max:"$price"}
				
		}
	}
]);

db.fruits.aggregate
([
	{
		$group:
		{
			_id:"$supplier_id",
			minPrice:{$min:"$price"}
				
		}
	}
]);


